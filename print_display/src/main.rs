/*
 *   ____                        ___
 *  /\  _`\   __                /\_ \
 *  \ \ \/\ \/\_\    ____  _____\//\ \      __     __  __
 *   \ \ \ \ \/\ \  /',__\/\ '__`\\ \ \   /'__`\  /\ \/\ \
 *    \ \ \_\ \ \ \/\__, `\ \ \L\ \\_\ \_/\ \L\.\_\ \ \_\ \
 *     \ \____/\ \_\/\____/\ \ ,__//\____\ \__/.\_\\/`____ \
 *      \/___/  \/_/\/___/  \ \ \/ \/____/\/__/\/_/ `/___/> \
 *                           \ \_\                     /\___/
 *                            \/_/                     \/__/
 *   ______                   __
 *  /\__  _\               __/\ \__
 *  \/_/\ \/ _ __    __   /\_\ \ ,_\
 *     \ \ \/\`'__\/'__`\ \/\ \ \ \/
 *      \ \ \ \ \//\ \L\.\_\ \ \ \ \_
 *       \ \_\ \_\\ \__/.\_\\ \_\ \__\
 *        \/_/\/_/ \/__/\/_/ \/_/\/__/
 *
 */

// import 'fmt'
use std::fmt;

// create a structure for 'Complex' numbers with 2 parts
//	derive 'Debug' for having a way to print the Complex
#[derive(Debug)]
struct Complex {
	real: f64,
	imag: f64,
}

// implement 'Display' trait so that it has a better way to print the data
impl fmt::Display for Complex {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		// Customize so that we can print the number like we would write in real life
		write!(f, "{} + {}i", self.real, self.imag)
	}
}

fn main() {
	let imaginary_number = Complex{ real: 3.3, imag: 7.2};
    println!("Display: {}", imaginary_number);
	println!("Debug: {:?}", imaginary_number);
}
